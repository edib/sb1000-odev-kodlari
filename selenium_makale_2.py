from selenium import webdriver
import time
from tqdm import tqdm
from bs4 import BeautifulSoup
browser = webdriver.Firefox()

url = "https://www.sciencedirect.com/search?pub=Computers%20%26%20Education&cid=271849&qs=%22game%20based%20learning%22&years=2023%2C2022%2C2021%2C2020%2C2019&show=100"

browser.get(url)

time.sleep(10)

# #rk4207 > a:nth-child(2) > span:nth-child(1)
elements = browser.find_elements_by_css_selector(".result-item-content")
# title-S0360131522001774 > span:nth-child(1) > span:nth-child(1) > em:nth-child(1)

with open('science-direct4.txt','w') as dn:
    for element in tqdm(elements):
        el = element.get_attribute("innerHTML")
        dn.write(F"{el}\n")

browser.close()