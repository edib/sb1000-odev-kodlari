import time
from tqdm import tqdm
from bs4 import BeautifulSoup



with open("science-direct4.txt", "r") as input:
    with open('science-direct5.txt','w') as dn:
        for element in tqdm(input):
            title = BeautifulSoup(element, "lxml").find('span', class_="anchor-text") # soup.find_all("a", class_="sister")
            a = BeautifulSoup(element, "lxml").find('a', class_="anchor")
            journal_date = BeautifulSoup(element, "lxml").find('span', class_="srctitle-date-fields").parent.find_next('span').contents[1]
            authors = []
            for author in BeautifulSoup(element, "lxml").find('ol', class_="Authors").findChildren():
                authors.append(author.get_text())
            authors = ','.join(map(str,authors))

            dn.write(F"'{title.get_text()}','{a['href']}', '{journal_date.get_text()}','{authors}' \n")